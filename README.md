## Reinforced turret Factorio mod

![](ReinforcedTurret_0.17.1/thumbnail.png)


Adds a new turret with better resistances, and that provides better damage.

#### Latest version
---
#### *Version: 0.17.1*

Changes:

    - initial release.
    - tweaked graphics from source
    - added properties for making it more balanced with vanilla turrets
    