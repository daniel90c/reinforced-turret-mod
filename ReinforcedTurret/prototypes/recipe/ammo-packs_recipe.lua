data:extend(
{
    {
        type = "recipe",
        name = "firearm-magazine-pack",
        enabled = false,
        energy_required = 20,
        ingredients = {
            {"iron-plate", 10},
            {"firearm-magazine", 10}
    },
        result = "firearm-magazine-pack",
        result_count = 1
    },
    {
    type = "recipe",
    name = "piercing-rounds-magazine-pack",
    enabled = false,
    energy_required = 30,
    ingredients = {
        {"iron-plate", 10},
        {"steel-plate", 2},
        {"piercing-rounds-magazine", 10}
    },
    result = "piercing-rounds-magazine-pack",
    result_count = 1
    },
    {
    type = "recipe",
    name = "uranium-rounds-magazine-pack",
    enabled = false,
    energy_required = 60,
    ingredients = {
        {"iron-plate", 20},
        {"steel-plate", 5},
        {"uranium-rounds-magazine", 10}
    },
    result = "uranium-rounds-magazine-pack",
    result_count = 1
  }
  }
)