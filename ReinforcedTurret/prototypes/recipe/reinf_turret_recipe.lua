data:extend(
{
    {
        type = "recipe",
        name = "reinf-gun-turret",
        enabled = false,
        energy_required = 20,
        ingredients =
        {
            {"iron-gear-wheel", 10},
            {"copper-plate", 10},
            {"iron-stick", 5},
            {"steel-plate", 10},
            --{"stone-brick", 5},
            {"gun-turret", 1}
        },
        result = "reinf-gun-turret"
    }
}
)
