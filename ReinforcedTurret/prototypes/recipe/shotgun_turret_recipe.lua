data:extend(
{
    {
        type = "recipe",
        name = "shotgun-turret",
        enabled = false,
        energy_required = 20,
        ingredients =
        {
            {"iron-gear-wheel", 10},
            {"copper-plate", 15},
            {"iron-plate", 15},
            --{"steel-plate", 10},
            --{"stone-brick", 5},
            --{"gun-turret", 1}
        },
        result = "shotgun-turret"
    }
}
)
