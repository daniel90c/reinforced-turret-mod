data:extend({  
	{
	  type = "corpse",
	  name = "reinf-turret-remnants",
	  icon = "__ReinforcedTurret__/graphics/icons/gun-turret.png",
	  icon_size = 32, icon_mipmaps = 4,
	  flags = {"placeable-neutral", "not-on-map"},
	  selection_box = {{-1, -1}, {1, 1}},
	  tile_width = 2,
	  tile_height = 2,
	  selectable_in_game = false,
	  subgroup = "remnants",
	  order="d[remnants]-a[generic]-a[small]",
	  time_before_removed = 60 * 60 * 15, -- 15 minutes
	  final_render_layer = "remnants",
	  remove_on_tile_placement = false,
	  animation = make_rotated_animation_variations_from_sheet (3,
	  {
		layers =
		{
		  {
			filename = "__ReinforcedTurret__/graphics/entity/gun-turret/remnants/gun-turret-remnants.png",
			line_length = 1,
			width = 126,
			height = 122,
			frame_count = 1,
			variation_count = 1,
			axially_symmetrical = false,
			direction_count = 1,
			shift = util.by_pixel(3, -1),
			hr_version =
			{
			  filename = "__ReinforcedTurret__/graphics/entity/gun-turret/remnants/hr-gun-turret-remnants.png",
			  line_length = 1,
			  width = 252,
			  height = 242,
			  frame_count = 1,
			  variation_count = 1,
			  axially_symmetrical = false,
			  direction_count = 1,
			  shift = util.by_pixel(3, -1.5),
			  scale = 0.5,
			}
		  },
		  {
			priority = "low",
			filename = "__ReinforcedTurret__/graphics/entity/gun-turret/remnants/mask/gun-turret-remnants-mask.png",
			width = 34,
			height = 32,
			frame_count = 1,
			--tint = { r = 0.869, g = 0.5  , b = 0.130, a = 0.5 },
			apply_runtime_tint = true,
			direction_count = 1,
			shift = util.by_pixel(-1, -11),
			hr_version=
			{
			  priority = "low",
			  filename = "__ReinforcedTurret__/graphics/entity/gun-turret/remnants/mask/hr-gun-turret-remnants-mask.png",
			  width = 68,
			  height = 64,
			  frame_count = 1,
			  --tint = { r = 0.869, g = 0.5  , b = 0.130, a = 0.5 },
			  apply_runtime_tint = true,
			  direction_count = 1,
			  shift = util.by_pixel(-1, -11),
			  scale = 0.5,
			}
		  }
		}
	  })
	}
  })