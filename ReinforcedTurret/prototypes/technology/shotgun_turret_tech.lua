data:extend(
{
    {
        type = "technology",
        name = "shotgun-turrets",
        icon = "__ReinforcedTurret__/graphics/technology/reinforced-gun-turret-research.png",
		    icon_size = 128,
        effects =
        {
            {
                type = "unlock-recipe",
                recipe = "shotgun-turret"
            },
        },
        prerequisites = {"military"},
        unit =
        {
            count = 10,
            ingredients =
            {
                {"automation-science-pack", 1},
                --{"logistic-science-pack", 1},
                --{"military-science-pack", 1}
            },
            time = 30
        },
        upgrade = true,
        order = "e-l-a"
    }
}
)

--- damages for  shotgun turrets
table.insert(
   data.raw["technology"]["physical-projectile-damage-1"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.2"
      }
)

table.insert(
   data.raw["technology"]["physical-projectile-damage-2"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.2"
      }
)
table.insert(
   data.raw["technology"]["physical-projectile-damage-3"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.2"
      }
)
table.insert(
   data.raw["technology"]["physical-projectile-damage-4"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.2"
      }
)
table.insert(
   data.raw["technology"]["physical-projectile-damage-5"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.2"
      }
)
table.insert(
   data.raw["technology"]["physical-projectile-damage-6"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.4"
      }
)
table.insert(
   data.raw["technology"]["physical-projectile-damage-7"].effects,
   {
        type = "turret-attack",
        turret_id = "shotgun-turret",
        modifier = "0.7"
      }
)