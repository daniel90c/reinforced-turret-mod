data:extend(
{
    {
        type = "technology",
        name = "ammo-packs",
        icon = "__ReinforcedTurret__/graphics/technology/ammo-packs-research.png",
		    icon_size = 128,
        effects =
        {
            {
                type = "unlock-recipe",
                recipe = "firearm-magazine-pack"
            },
        },
        prerequisites = {"military-2", "turrets"},
        unit =
        {
            count = 100,
            ingredients =
            {
                {"automation-science-pack", 1},
                {"logistic-science-pack", 1},
                --{"military-science-pack", 1}
            },
            time = 30
        },
        --upgrade = true,
        order = "a-j-a"
    }
}
)
---ammo-packs 2
data:extend(
{
    {
        type = "technology",
        name = "ammo-packs2",
        icon = "__ReinforcedTurret__/graphics/technology/piercing-rounds-magazine-pack.png",
		    icon_size = 128,
        effects =
        {
            {
                type = "unlock-recipe",
                recipe = "piercing-rounds-magazine-pack"
            },
        },
        prerequisites = {"military-2", "military-science-pack"},
        unit =
        {
            count = 100,
            ingredients =
            {
                {"automation-science-pack", 1},
                {"logistic-science-pack", 1},
                --{"military-science-pack", 1}
            },
            time = 60
        },
        --upgrade = true,
        order = "a-j-a"
    }
}
)
---ammo-packs 3
data:extend(
{
    {
        type = "technology",
        name = "ammo-packs3",
        icon = "__ReinforcedTurret__/graphics/technology/uranium-rounds-magazine-pack.png",
		    icon_size = 128,
        effects =
        {
            {
                type = "unlock-recipe",
                recipe = "uranium-rounds-magazine-pack"
            },
        },
        prerequisites = {"military-3", "uranium-ammo"},
        unit =
        {
          count = 1000,
          ingredients =
          {
            {"automation-science-pack", 1},
            {"logistic-science-pack", 1},
            {"chemical-science-pack", 1},
            {"military-science-pack", 1},
            {"utility-science-pack", 1}
          },
          time = 45
        },
        order = "e-a-b"
      },
      
}
)
