data:extend(
{
    {
        type = "item",
        name = "reinf-gun-turret",
        icon = "__ReinforcedTurret__/graphics/icons/reinf-gun-turret.png",
		icon_size = 32,
        flags = {},
        subgroup = "defensive-structure",
        order = "b[turret]-b[reinforced-gun-turret]",
        place_result = "reinf-gun-turret",
        enable = false,
        stack_size = 50
    }
}
)