data:extend(
{
    {
    type = "ammo",
    name = "firearm-magazine-pack",
    icon = "__ReinforcedTurret__/graphics/icons/firearm-magazine-pack-50cal.png",
    icon_size = 32,
    --flags = {"goes-to-main-inventory"},
    ammo_type =
    {
      category = "bullet",
      action =
      {
        {
          type = "direct",
          action_delivery =
          {
            {
              type = "instant",
              source_effects =
              {
                {
                  type = "create-explosion",
                  entity_name = "explosion-gunshot"
                }
              },
              target_effects =
              {
                {
                  type = "create-entity",
                  entity_name = "explosion-hit"
                },
                {
                  type = "damage",
                  damage = { amount = 5 , type = "physical"}
                }
              }
            }
          }
        }
      }
    },
    magazine_size = 100,
    subgroup = "ammo",
    order = "a[basic-clips]-d[firearm-magazine-pack]",
    stack_size = 200
  },
  ---
   {
    type = "ammo",
    name = "piercing-rounds-magazine-pack",
    icon = "__ReinforcedTurret__/graphics/icons/piercing-rounds-magazine-pack-50cal.png",
    icon_size = 32,
    --flags = {"goes-to-main-inventory"},
    ammo_type =
    {
      category = "bullet",
      action =
      {
        type = "direct",
        action_delivery =
        {
          type = "instant",
          source_effects =
          {
            type = "create-explosion",
            entity_name = "explosion-gunshot"
          },
          target_effects =
          {
            {
              type = "create-entity",
              entity_name = "explosion-hit"
            },
            {
              type = "damage",
              damage = { amount = 8, type = "physical"}
            }
          }
        }
      }
    },
    magazine_size = 100,
    subgroup = "ammo",
    order = "a[basic-clips]-e[piercing-rounds-magazine-pack]",
    stack_size = 200
  },
 ----- uranium pack
  {
    type = "ammo",
    name = "uranium-rounds-magazine-pack",
    icon = "__ReinforcedTurret__/graphics/icons/uranium-rounds-magazine-pack-50cal.png",
    icon_size = 32,
    --flags = {"goes-to-main-inventory"},
    ammo_type =
    {
      category = "bullet",
      action =
      {
        type = "direct",
        action_delivery =
        {
          type = "instant",
          source_effects =
          {
            type = "create-explosion",
            entity_name = "explosion-gunshot"
          },
          target_effects =
          {
            {
              type = "create-entity",
              entity_name = "explosion-hit"
            },
            {
              type = "damage",
              damage = { amount = 24, type = "physical"}
            }
          }
        }
      }
    },
    magazine_size = 100,
    subgroup = "ammo",
    order = "a[basic-clips]-f[uranium-rounds-magazine-pack]",
    stack_size = 200
  }
  }
)