data:extend(
{
    {
        type = "item",
        name = "shotgun-turret",
        icon = "__ReinforcedTurret__/graphics/icons/reinf-gun-turret.png",
		icon_size = 32,
        flags = {},
        subgroup = "defensive-structure",
        order = "b[turret]-c[shotgun-turret]",
        place_result = "shotgun-turret",
        enable = false,
        stack_size = 50
    }
}
)