---
--- reinforce turret
---
require("prototypes.item.reinf_turret_item")
require("prototypes.recipe.reinf_turret_recipe")
require("prototypes.technology.reinf_turret_tech")
require("prototypes.entity.reinf_turret_remnants")
require("prototypes.entity.reinf_turret_entity")
---
--- shotgun turret
---
require("prototypes.item.shotgun_turret_item")
require("prototypes.recipe.shotgun_turret_recipe")
require("prototypes.technology.shotgun_turret_tech")
require("prototypes.entity.shotgun_turret_remnants")
require("prototypes.entity.shotgun_turret_entity")
---
--- ammo packs
---
require("prototypes.item.ammo-packs_item")
require("prototypes.recipe.ammo-packs_recipe")
require("prototypes.technology.ammo-packs_tech")